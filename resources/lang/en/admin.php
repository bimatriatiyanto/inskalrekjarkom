<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'name' => 'Name',
            'username' => 'Username',
            'email' => 'Email',
            'nip' => 'NIP',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',

            //Belongs to many relations
            'roles' => 'Roles',

        ],
    ],

    'unit' => [
        'title' => 'Units',

        'actions' => [
            'index' => 'Units',
            'create' => 'New Unit',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'unit_kerja' => 'Unit kerja',

        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
